import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;
import controlP5.*;

// ARRAYS OF OBSTACLES
ArrayList<Bird> birds;
ArrayList<Iceberg> icebergs;
ArrayList<Asteroid> asteroids;

// IMAGES
PImage birdImage, icebergImage, backgroundImage, waveImage, asteroidImage, shipImage, planeImage, spaceshipImage;

// OBJECTS
Wave waveBack, waveFront;
Background bg;
Ship ship;

// MINIM
Minim minim;
AudioInput in;

// CONTROLP5
ControlP5 cp5;
Knob myKnob;

void setup()
{
  // OBJECTS ARRAYS SETUP
  birds = new ArrayList<Bird>();
  icebergs = new ArrayList<Iceberg>();
  asteroids = new ArrayList<Asteroid>();

  // MAIN SCREEN SETUP
  size(1200, 600, P3D);

  // SETTING UP MICROPHONE IN
  minim = new Minim(this);
  in = minim.getLineIn();

  // CP5 SETUP
  cp5 = new ControlP5(this);
  myKnob = cp5.addKnob("SCREAM-O-METER")
    .setRange(0, 100)
    .setValue(currentVolume()*8)
    .setPosition(30, 30)
    .setRadius(50)
    .setDragDirection(Knob.HORIZONTAL);

  // IMAGE SETUP
  birdImage = loadImage("bird.png");
  icebergImage = loadImage("iceberg.png");
  backgroundImage = loadImage("background.png");
  waveImage = loadImage("waves.png");
  asteroidImage = loadImage("asteroid.png");
  shipImage = loadImage("ship.png");
  planeImage = loadimage("plane.png");

  imageMode(CENTER);

  // INSTANTIATING OBJECTS
  bg = new Background(width / 2, 0, backgroundImage);
  waveFront = new Wave(400, 700, waveImage);
  waveBack = new Wave(800, 600, waveImage);
  ship = new Ship(100, 400, shipImage);

  background(0);

  frameRate(30);

  //translateEverything(600);
}

void draw()
{
  //println(String.format("%.2f", currentVolume()));

  myKnob.setValue(currentVolume());

  drawBackground();
  drawWaves(true);
  drawShip();
  spawnObjects();
  drawObjects();
  drawWaves(false);
  moveShip();
}

// RETURNS CURRENT VOLUME
float currentVolume()
{
  return in.left.level() * 800;
}

void drawBackground()
{
  image(bg.image, bg.positionX, bg.positionY);
  image(bg.image, bg.positionX + width, bg.positionY);

  if (bg.positionX <= -(width / 2))
  {
    bg.positionX = width / 2;
  }

  bg.positionX --;
}

Boolean goingUp = false;
int counter = 0, counterMax = 60;

void drawWaves(boolean back)
{
  if (goingUp)
  {
    waveBack.positionY -= 2;
    waveFront.positionY ++;
    counter++;

    if (counter >= counterMax)
    {
      counter = 0;
      goingUp = false;
    }
  } else
  {
    waveBack.positionY += 2;
    waveFront.positionY --;

    counter++;

    if (counter >= counterMax)
    {
      counter = 0;
      goingUp = true;
    }
  }

  waveBack.positionX --;
  waveFront.positionX --;

  if (waveBack.positionX < -800)
  {
    waveBack.positionX = 800;
  }

  if (waveFront.positionX < -800)
  {
    waveFront.positionX = 800;
  }

  if (back)
  {
    image(waveBack.image, waveBack.positionX, waveBack.positionY);
    image(waveBack.image, waveBack.positionX + 1600, waveBack.positionY);
  } else
  {
    image(waveFront.image, waveFront.positionX, waveFront.positionY);
    image(waveFront.image, waveFront.positionX + 1600, waveFront.positionY);
  }
}

void drawShip()
{
  image(ship.image, ship.positionX, ship.positionY);
}

void drawObjects()
{
  for (Bird b : birds)
  {
    image(b.image, b.positionX, b.positionY);
    b.positionX -= 5;
  }

  for (Iceberg i : icebergs)
  {
    image(i.image, i.positionX, i.positionY);
    i.positionX -= 3;
  }

  for (Asteroid a : asteroids)
  {
    image(a.image, a.positionX, a.positionY);
    a.positionX -= 6;
  }
}

int birdSpawnCounter = 210;
int icebergSpawnCounter = 450;
int asteroidSpawnCounter = 400;

int birdSpawnTime = 120;
int icebergSpawnTime = 500;
int asteroidSpawnTime = 200;

void spawnObjects()
{
  birdSpawnCounter ++;
  icebergSpawnCounter ++;
  asteroidSpawnCounter ++;

  if (birdSpawnCounter > birdSpawnTime)
  {
    birds.add(new Bird(1600, (int)random(100, 400) + translatedOffset, birdImage));
    birdSpawnCounter = 0;
    birdSpawnTime = (int)random(90, 250);
  }

  if (icebergSpawnCounter > icebergSpawnTime)
  {
    icebergs.add(new Iceberg(1400, 550 + translatedOffset, icebergImage));
    icebergSpawnCounter = 0;
    icebergSpawnTime = (int)random(400, 1000);
  }

  if (asteroidSpawnCounter > asteroidSpawnTime)
  {
    asteroids.add(new Asteroid(1400, -(int)random(100, 550) + translatedOffset, asteroidImage));
    asteroidSpawnCounter = 0;
    asteroidSpawnTime = (int)random(200, 600);
  }
}

int translatedOffset = 0;

void translateEverything(int amount)
{
  translatedOffset += amount;

  waveFront.moveVertical(amount);
  waveBack.moveVertical(amount);
  bg.moveVertical(amount);
  ship.moveVertical(amount);

  for (Bird b : birds)
  {
    b.moveVertical(amount);
  }

  for (Iceberg i : icebergs)
  {
    i.moveVertical(amount);
  }

  for (Asteroid a : asteroids)
  {
    a.moveVertical(amount);
  }
}

boolean gravityInEffect = true;
int idleTimer = 0;
int idleTime = 15;
boolean movingUp = false;
int movingUpTimer = 0;
int movingUpTime = 15;

void moveShip()
{
  idleTimer ++;
  if (idleTimer > idleTime)
  {
    gravityInEffect = true;
  }

  if (currentVolume() > 40)
  {
    idleTimer = 0;
    movingUp = true;
    gravityInEffect = false;
  }

  if (movingUp)
  {
    movingUpTimer ++;

    if (movingUpTimer > movingUpTime)
    {
      movingUp = false;
      movingUpTimer = 0;
    }

    if (ship.positionY - translatedOffset > -500)
    {
      ship.moveVertical(-3);
      idleTimer = 0;

      if (translatedOffset < 600 && ship.positionY < 300)
      {
        translateEverything(3);
      }
    }
  }

  if (gravityInEffect)
  {
    movingUpTimer = 0;

    if (ship.positionY - translatedOffset < 500)
    {
      ship.moveVertical(3);

      if (translatedOffset > 0 && ship.positionY - translatedOffset > -300)
      {
        translateEverything(-3);
      }
    }
  }
  println("SHIP: " + (ship.positionY - translatedOffset) + " | TR: " + translatedOffset);
}

void keyPressed()
{
  if (key == 'w' && (translatedOffset + 25 <= 600))
  {
    translateEverything(25);
  }

  if (key == 's' && (translatedOffset - 25 >= 0))
  {
    translateEverything(-25);
  }
}