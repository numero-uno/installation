class Bird
{
  PImage image;
  
  int positionX = 0;
  int positionY = 0;
  
  public Bird(int x, int y, PImage img)
  {
    positionX = x;
    positionY = y;
    image = img;
  }
  
  public void moveVertical(int amount)
  {
    positionY += amount;
  }
}